Functions changes summary: 0 Removed, 1 Changed, 0 Added function
Variables changes summary: 0 Removed, 0 Changed, 0 Added variable

1 function with some indirect sub-type change:

  [C] 'function opaque* fun(type&)' at test-PR31646-v0.cc:15:1 has some indirect sub-type changes:
    return type changed:
      entity changed from 'opaque*' to 'void'
      type size changed from 64 to 0 (in bits)
    parameter 1 of type 'type&' has sub-type changes:
      in referenced type 'struct type' at test-PR31646-v1.cc:8:1:
        type size hasn't changed
        1 data member change:
          type of 'opaque* m1' changed:
            in pointed to type 'struct opaque' at test-PR31646-v1.cc:1:1:
              type size changed from 64 to 96 (in bits)
              1 data member insertion:
                'char m_inserted', at offset 32 (in bits) at test-PR31646-v1.cc:4:1
              1 data member change:
                'int m1' offset changed from 32 to 64 (in bits) (by +32 bits)


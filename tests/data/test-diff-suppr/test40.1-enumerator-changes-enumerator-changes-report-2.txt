Functions changes summary: 0 Removed, 2 Changed (1 filtered out), 0 Added functions
Variables changes summary: 0 Removed, 0 Changed, 0 Added variable

2 functions with some indirect sub-type change:

  [C] 'function void fun0(Enum0)' at test40.1-enumerator-changes-enumerator-changes-v0.c:25:1 has some indirect sub-type changes:
    parameter 1 of type 'enum Enum0' has sub-type changes:
      type size hasn't changed
      1 enumerator insertion:
        'Enum0::ENUM0_E1' value '1'
      1 enumerator change:
        'Enum0::ENOM0_LAST' from value '1' to '2' at test40.1-enumerator-changes-enumerator-changes-v1.c:6:1

  [C] 'function void fun2(Enum2)' at test40.1-enumerator-changes-enumerator-changes-v0.c:35:1 has some indirect sub-type changes:
    parameter 1 of type 'enum Enum2' has sub-type changes:
      type size hasn't changed
      1 enumerator insertion:
        'Enum2::ENUM2_E1' value '1'
      1 enumerator change:
        'Enum2::LAST_ENUM1_ENUMERATOR' from value '1' to '2' at test40.1-enumerator-changes-enumerator-changes-v1.c:20:1


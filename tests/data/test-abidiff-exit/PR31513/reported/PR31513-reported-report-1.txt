Functions changes summary: 16 Removed, 0 Changed (22 filtered out), 21 Added functions
Variables changes summary: 0 Removed, 0 Changed, 0 Added variable
Function symbols changes summary: 2 Removed, 2 Added function symbols not referenced by debug info
Variable symbols changes summary: 0 Removed, 0 Added variable symbol not referenced by debug info

16 Removed functions:

  [D] 'method void __gnu_cxx::new_allocator<char>::destroy<char>(char*)'    {_ZN9__gnu_cxx13new_allocatorIcE7destroyIcEEvPT_}
  [D] 'method char* std::__copy_move<true, true, std::random_access_iterator_tag>::__copy_m<char>(const char*, const char*, char*)'    {_ZNSt11__copy_moveILb1ELb1ESt26random_access_iterator_tagE8__copy_mIcEEPT_PKS3_S6_S4_}
  [D] 'function char* std::__copy_move_a2<true, char*, char*>(char*, char*, char*)'    {_ZSt14__copy_move_a2ILb1EPcS0_ET1_T0_S2_S1_}
  [D] 'function char* std::__copy_move_a<true, char*, char*>(char*, char*, char*)'    {_ZSt13__copy_move_aILb1EPcS0_ET1_T0_S2_S1_}
  [D] 'function std::move_iterator<char*> std::__make_move_if_noexcept_iterator<char>(char*)'    {_ZSt32__make_move_if_noexcept_iteratorIcSt13move_iteratorIPcEET0_PT_}
  [D] 'function char* std::__miter_base<char*>(char*)'    {_ZSt12__miter_baseIPcET_S1_}
  [D] 'function char* std::__miter_base<char*>(std::move_iterator<char*>)'    {_ZSt12__miter_baseIPcEDTcl12__miter_basecldtfp_4baseEEESt13move_iteratorIT_E}
  [D] 'method char* std::__uninitialized_copy<true>::__uninit_copy<std::move_iterator<char*>, char*>(std::move_iterator<char*>, std::move_iterator<char*>, char*)'    {_ZNSt20__uninitialized_copyILb1EE13__uninit_copyISt13move_iteratorIPcES3_EET0_T_S6_S5_}
  [D] 'function char* std::__uninitialized_copy_a<std::move_iterator<char*>, char*, char>(std::move_iterator<char*>, std::move_iterator<char*>, char*, std::allocator<char>&)'    {_ZSt22__uninitialized_copy_aISt13move_iteratorIPcES1_cET0_T_S4_S3_RSaIT1_E}
  [D] 'function char* std::__uninitialized_move_if_noexcept_a<char*, char*, std::allocator<char> >(char*, char*, char*, std::allocator<char>&)'    {_ZSt34__uninitialized_move_if_noexcept_aIPcS0_SaIcEET0_T_S3_S2_RT1_}
  [D] 'method void std::allocator_traits<std::allocator<char> >::destroy<char>(std::allocator_traits<std::allocator<char> >::allocator_type&, char*)'    {_ZNSt16allocator_traitsISaIcEE7destroyIcEEvRS0_PT_}
  [D] 'function char* std::copy<std::move_iterator<char*>, char*>(std::move_iterator<char*>, std::move_iterator<char*>, char*)'    {_ZSt4copyISt13move_iteratorIPcES1_ET0_T_S4_S3_}
  [D] 'method std::move_iterator<char*>::iterator_type std::move_iterator<char*>::base() const'    {_ZNKSt13move_iteratorIPcE4baseEv}
  [D] 'method void std::move_iterator<char*>::move_iterator(std::move_iterator<char*>::iterator_type)'    {_ZNSt13move_iteratorIPcEC2ES0_, aliases _ZNSt13move_iteratorIPcEC1ES0_}
  [D] 'function char* std::uninitialized_copy<std::move_iterator<char*>, char*>(std::move_iterator<char*>, std::move_iterator<char*>, char*)'    {_ZSt18uninitialized_copyISt13move_iteratorIPcES1_ET0_T_S4_S3_}
  [D] 'method void std::vector<char, std::allocator<char> >::emplace_back<char>()'    {_ZNSt6vectorIcSaIcEE12emplace_backIJcEEEvDpOT_}

21 Added functions:

  [A] 'function bool __gnu_cxx::__is_null_pointer<char const>(const char*)'    {_ZN9__gnu_cxx17__is_null_pointerIKcEEbPT_}
  [A] 'method __gnu_cxx::__normal_iterator<char*, std::vector<char, std::allocator<char> > >::reference __gnu_cxx::__normal_iterator<char*, std::vector<char, std::allocator<char> > >::operator*() const'    {_ZNK9__gnu_cxx17__normal_iteratorIPcSt6vectorIcSaIcEEEdeEv}
  [A] 'method __gnu_cxx::__normal_iterator<char*, std::vector<char, std::allocator<char> > > __gnu_cxx::__normal_iterator<char*, std::vector<char, std::allocator<char> > >::operator-(__gnu_cxx::__normal_iterator<char*, std::vector<char, std::allocator<char> > >::difference_type) const'    {_ZNK9__gnu_cxx17__normal_iteratorIPcSt6vectorIcSaIcEEEmiEl}
  [A] 'method bool __gnu_cxx::char_traits<char>::eq(const __gnu_cxx::char_traits<char>::char_type&, const __gnu_cxx::char_traits<char>::char_type&)'    {_ZN9__gnu_cxx11char_traitsIcE2eqERKcS3_}
  [A] 'method std::size_t __gnu_cxx::char_traits<char>::length(const __gnu_cxx::char_traits<char>::char_type*)'    {_ZN9__gnu_cxx11char_traitsIcE6lengthEPKc}
  [A] 'method __gnu_cxx::new_allocator<char>::size_type __gnu_cxx::new_allocator<char>::_M_max_size() const'    {_ZNK9__gnu_cxx13new_allocatorIcE11_M_max_sizeEv}
  [A] 'method std::_Vector_base<char, std::allocator<char> >::_Vector_impl_data::_Vector_impl_data()'    {_ZNSt12_Vector_baseIcSaIcEE17_Vector_impl_dataC2Ev, aliases _ZNSt12_Vector_baseIcSaIcEE17_Vector_impl_dataC1Ev}
  [A] 'method void std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_M_construct<char const*>(const char*, const char*, std::forward_iterator_tag)'    {_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag}
  [A] 'method void std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::basic_string<>(const char*, const std::allocator<char>&)'    {_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2IS3_EEPKcRKS3_, aliases _ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1IS3_EEPKcRKS3_}
  [A] 'function std::iterator_traits<char const*>::difference_type std::__distance<char const*>(const char*, const char*, std::random_access_iterator_tag)'    {_ZSt10__distanceIPKcENSt15iterator_traitsIT_E15difference_typeES3_S3_St26random_access_iterator_tag}
  [A] 'function std::iterator_traits<char const*>::iterator_category std::__iterator_category<char const*>(const char* const&)'    {_ZSt19__iterator_categoryIPKcENSt15iterator_traitsIT_E17iterator_categoryERKS3_}
  [A] 'function char* std::__relocate_a<char*, char*, std::allocator<char> >(char*, char*, char*, std::allocator<char>&)'    {_ZSt12__relocate_aIPcS0_SaIcEET0_T_S3_S2_RT1_}
  [A] 'function std::__enable_if_t std::__relocate_a_1<char, char>(char*, char*, char*, std::allocator<char>&)'    {_ZSt14__relocate_a_1IccENSt9enable_ifIXsrSt24__is_bitwise_relocatableIT_vE5valueEPS2_E4typeES4_S4_S4_RSaIT0_E}
  [A] 'method std::size_t std::char_traits<char>::length(const std::char_traits<char>::char_type*)'    {_ZNSt11char_traitsIcE6lengthEPKc}
  [A] 'function std::iterator_traits<char const*>::difference_type std::distance<char const*>(const char*, const char*)'    {_ZSt8distanceIPKcENSt15iterator_traitsIT_E15difference_typeES3_S3_}
  [A] 'function const unsigned long int& std::min<long unsigned int>(const unsigned long int&, const unsigned long int&)'    {_ZSt3minImERKT_S2_S2_}
  [A] 'method std::vector<char, std::allocator<char> >::pointer std::vector<char, std::allocator<char> >::_S_do_relocate(std::vector<char, std::allocator<char> >::pointer, std::vector<char, std::allocator<char> >::pointer, std::vector<char, std::allocator<char> >::pointer, std::vector<char, std::allocator<char> >::_Tp_alloc_type&, std::true_type)'    {_ZNSt6vectorIcSaIcEE14_S_do_relocateEPcS2_S2_RS0_St17integral_constantIbLb1EE}
  [A] 'method std::vector<char, std::allocator<char> >::size_type std::vector<char, std::allocator<char> >::_S_max_size(const std::vector<char, std::allocator<char> >::_Tp_alloc_type&)'    {_ZNSt6vectorIcSaIcEE11_S_max_sizeERKS0_}
  [A] 'method std::vector<char, std::allocator<char> >::pointer std::vector<char, std::allocator<char> >::_S_relocate(std::vector<char, std::allocator<char> >::pointer, std::vector<char, std::allocator<char> >::pointer, std::vector<char, std::allocator<char> >::pointer, std::vector<char, std::allocator<char> >::_Tp_alloc_type&)'    {_ZNSt6vectorIcSaIcEE11_S_relocateEPcS2_S2_RS0_}
  [A] 'method std::vector<char, std::allocator<char> >::reference std::vector<char, std::allocator<char> >::back()'    {_ZNSt6vectorIcSaIcEE4backEv}
  [A] 'method std::vector<char, std::allocator<char> >::reference std::vector<char, std::allocator<char> >::emplace_back<char>()'    {_ZNSt6vectorIcSaIcEE12emplace_backIJcEEERcDpOT_}

2 Removed function symbols not referenced by debug info:

  [D] _fini
  [D] _init

2 Added function symbols not referenced by debug info:

  [A] _ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_Alloc_hiderD1Ev, aliases _ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_Alloc_hiderD2Ev
  [A] _ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_Alloc_hiderD2Ev


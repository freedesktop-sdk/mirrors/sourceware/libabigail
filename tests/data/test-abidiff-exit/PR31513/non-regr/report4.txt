Functions changes summary: 0 Removed, 1 Changed, 0 Added function
Variables changes summary: 0 Removed, 0 Changed, 0 Added variable

1 function with some indirect sub-type change:

  [C] 'function int foo(type&)' at test4-v0.cc:10:1 has some indirect sub-type changes:
    parameter 1 of type 'type&' has sub-type changes:
      in referenced type 'struct type' at test4-v1.cc:8:1:
        type size changed from 64 to 96 (in bits)
        1 base class insertion:
          struct base at test4-v1.cc:2:1
        1 data member deletion:
          'char m1', at offset 32 (in bits) at test4-v0.cc:6:1
        1 data member insertion:
          'char m3', at offset 64 (in bits) at test4-v1.cc:10:1


Functions changes summary: 0 Removed, 0 Changed, 0 Added function
Variables changes summary: 0 Removed, 1 Changed, 0 Added variable

1 Changed variable:

  [C] 'int S::* pm' was changed to 'int T::* pm' at test-ptr-to-mbr2-v1.cc:6:1:
    type of variable changed:
      pointer-to-member type changed from: 'int S::* to: 'int T::*'
      in containing type 'struct S' of pointed-to-member type 'int S::*' at test-ptr-to-mbr2-v1.cc:1:1:
        type name changed from 'S' to 'T'
        type size hasn't changed
        no data member change (1 filtered);


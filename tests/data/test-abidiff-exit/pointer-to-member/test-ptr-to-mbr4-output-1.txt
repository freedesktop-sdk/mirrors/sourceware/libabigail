Functions changes summary: 0 Removed, 0 Changed, 0 Added function
Variables changes summary: 0 Removed, 1 Changed, 0 Added variable

1 Changed variable:

  [C] 'int S::* pm' was changed to 'double S::* pm' at test-ptr-to-mbr4-v1.cc:7:1:
    type of variable changed:
      pointer-to-member type changed from: 'int S::* to: 'double S::*'
      in data member type 'int' of pointed-to-member type 'int S::*':
        type name changed from 'int' to 'double'
        type size changed from 32 to 64 (in bits)


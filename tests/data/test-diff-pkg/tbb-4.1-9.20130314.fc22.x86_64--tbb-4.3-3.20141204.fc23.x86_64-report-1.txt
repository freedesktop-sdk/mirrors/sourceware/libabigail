================ changes of 'libtbb.so.2'===============
  Functions changes summary: 0 Removed, 2 Changed (97 filtered out), 17 Added functions
  Variables changes summary: 0 Removed, 0 Changed, 0 Added variable
  Function symbols changes summary: 0 Removed, 0 Added function symbol not referenced by debug info
  Variable symbols changes summary: 3 Removed, 0 Added variable symbols not referenced by debug info

  17 Added functions:

    [A] 'method int tbb::interface7::internal::task_arena_base::internal_current_slot(void)'    {_ZN3tbb10interface78internal15task_arena_base21internal_current_slotEv}
    [A] 'method void tbb::interface7::internal::task_arena_base::internal_enqueue(tbb::task&, intptr_t) const'    {_ZNK3tbb10interface78internal15task_arena_base16internal_enqueueERNS_4taskEl}
    [A] 'method void tbb::interface7::internal::task_arena_base::internal_execute(tbb::interface7::internal::delegate_base&) const'    {_ZNK3tbb10interface78internal15task_arena_base16internal_executeERNS1_13delegate_baseE}
    [A] 'method void tbb::interface7::internal::task_arena_base::internal_initialize()'    {_ZN3tbb10interface78internal15task_arena_base19internal_initializeEv}
    [A] 'method void tbb::interface7::internal::task_arena_base::internal_terminate()'    {_ZN3tbb10interface78internal15task_arena_base18internal_terminateEv}
    [A] 'method void tbb::interface7::internal::task_arena_base::internal_wait() const'    {_ZNK3tbb10interface78internal15task_arena_base13internal_waitEv}
    [A] 'method void tbb::interface8::internal::x86_rtm_rw_mutex::internal_acquire_reader(tbb::interface8::internal::x86_rtm_rw_mutex::scoped_lock&, bool)'    {_ZN3tbb10interface88internal16x86_rtm_rw_mutex23internal_acquire_readerERNS2_11scoped_lockEb}
    [A] 'method void tbb::interface8::internal::x86_rtm_rw_mutex::internal_acquire_writer(tbb::interface8::internal::x86_rtm_rw_mutex::scoped_lock&, bool)'    {_ZN3tbb10interface88internal16x86_rtm_rw_mutex23internal_acquire_writerERNS2_11scoped_lockEb}
    [A] 'method void tbb::interface8::internal::x86_rtm_rw_mutex::internal_construct()'    {_ZN3tbb10interface88internal16x86_rtm_rw_mutex18internal_constructEv}
    [A] 'method bool tbb::interface8::internal::x86_rtm_rw_mutex::internal_downgrade(tbb::interface8::internal::x86_rtm_rw_mutex::scoped_lock&)'    {_ZN3tbb10interface88internal16x86_rtm_rw_mutex18internal_downgradeERNS2_11scoped_lockE}
    [A] 'method void tbb::interface8::internal::x86_rtm_rw_mutex::internal_release(tbb::interface8::internal::x86_rtm_rw_mutex::scoped_lock&)'    {_ZN3tbb10interface88internal16x86_rtm_rw_mutex16internal_releaseERNS2_11scoped_lockE}
    [A] 'method bool tbb::interface8::internal::x86_rtm_rw_mutex::internal_try_acquire_writer(tbb::interface8::internal::x86_rtm_rw_mutex::scoped_lock&)'    {_ZN3tbb10interface88internal16x86_rtm_rw_mutex27internal_try_acquire_writerERNS2_11scoped_lockE}
    [A] 'method bool tbb::interface8::internal::x86_rtm_rw_mutex::internal_upgrade(tbb::interface8::internal::x86_rtm_rw_mutex::scoped_lock&)'    {_ZN3tbb10interface88internal16x86_rtm_rw_mutex16internal_upgradeERNS2_11scoped_lockE}
    [A] 'method void tbb::internal::concurrent_queue_base_v8::internal_push_move(void*)'    {_ZN3tbb8internal24concurrent_queue_base_v818internal_push_moveEPKv}
    [A] 'method bool tbb::internal::concurrent_queue_base_v8::internal_push_move_if_not_full(void*)'    {_ZN3tbb8internal24concurrent_queue_base_v830internal_push_move_if_not_fullEPKv}
    [A] 'method void tbb::internal::concurrent_queue_base_v8::move_content(tbb::internal::concurrent_queue_base_v8&)'    {_ZN3tbb8internal24concurrent_queue_base_v812move_contentERS1_}
    [A] 'method void tbb::task_group_context::capture_fp_settings()'    {_ZN3tbb18task_group_context19capture_fp_settingsEv}

  2 functions with some indirect sub-type change:

    [C] 'method tbb::task& tbb::internal::allocate_root_with_context_proxy::allocate(std::size_t) const' at task.cpp:74:1 has some indirect sub-type changes:
      implicit parameter 0 of type 'const tbb::internal::allocate_root_with_context_proxy*' has sub-type changes:
        in pointed to type 'const tbb::internal::allocate_root_with_context_proxy':
          in unqualified underlying type 'class tbb::internal::allocate_root_with_context_proxy' at task.h:131:1:
            type size hasn't changed
            1 data member change:
              type of 'task_group_context& my_context' changed:
                in referenced type 'class tbb::task_group_context' at task.h:302:1:
                  type size hasn't changed
                  1 data member insertion:
                    'tbb::internal::cpu_ctl_env_space my_cpu_ctl_env', at offset 896 (in bits) at task.h:380:1
                  1 data member changes (2 filtered):
                    type of 'char _leading_padding[80]' changed:
                      type name changed from 'char[80]' to 'char[72]'
                      array type size changed from 640 to 576
                      array type subrange 1 changed length from 80 to 72
                      array subrange changed: 
                        upper bound of '<anonymous range>[72]' change from '79' to '71'

    [C] 'function void tbb::internal::throw_exception_v4(tbb::internal::exception_id)' at tbb_misc.cpp:126:1 has some indirect sub-type changes:
      parameter 1 of type 'enum tbb::internal::exception_id' has sub-type changes:
        type size hasn't changed
        1 enumerator insertion:
          'tbb::internal::exception_id::eid_bad_tagged_msg_cast' value '20'
        1 enumerator change:
          'tbb::internal::exception_id::eid_max' from value '20' to '21' at tbb_exception.h:79:1

  3 Removed variable symbols not referenced by debug info:

    [D] _ZTVN3rml16versioned_objectE
    [D] _ZTVN3rml6clientE
    [D] _ZTVN3rml6serverE

================ end of changes of 'libtbb.so.2'===============

================ changes of 'libtbbmalloc.so.2'===============
  Functions changes summary: 9 Removed, 0 Changed, 0 Added functions
  Variables changes summary: 0 Removed, 0 Changed, 0 Added variable
  Function symbols changes summary: 0 Removed, 27 Added function symbols not referenced by debug info
  Variable symbols changes summary: 0 Removed, 0 Added variable symbol not referenced by debug info

  9 Removed functions:

    [D] 'function void* rml::internal::__TBB_internal_calloc(size_t, size_t)'    {__TBB_internal_calloc}
    [D] 'function void rml::internal::__TBB_internal_free(void*)'    {__TBB_internal_free}
    [D] 'function void* rml::internal::__TBB_internal_malloc(size_t)'    {__TBB_internal_malloc}
    [D] 'function int rml::internal::__TBB_internal_posix_memalign(void**, size_t, size_t)'    {__TBB_internal_posix_memalign}
    [D] 'function void* rml::internal::__TBB_internal_realloc(void*, size_t)'    {__TBB_internal_realloc}
    [D] 'function void* safer_scalable_aligned_realloc(void*, size_t, size_t, void*)'    {safer_scalable_aligned_realloc}
    [D] 'function void safer_scalable_free(void*, void (*)(void*))'    {safer_scalable_free}
    [D] 'function size_t safer_scalable_msize(void*, size_t (*)(void*))'    {safer_scalable_msize}
    [D] 'function void* safer_scalable_realloc(void*, size_t, void*)'    {safer_scalable_realloc}

  27 Added function symbols not referenced by debug info:

    [A] _Z10BitScanRevm
    [A] _Z10FencedLoadRVKl
    [A] _Z11FencedStoreRVll
    [A] _Z15AtomicIncrementRVl
    [A] _Z15SpinWaitWhileEqRVKll
    [A] _Z16AtomicFetchStorePVvm
    [A] _Z21AtomicCompareExchangeRVlll
    [A] _Z8AtomicOrPVvm
    [A] _Z8do_yieldv
    [A] _Z9AtomicAddRVll
    [A] _Z9AtomicAndPVvm
    [A] _ZN11MallocMutex11scoped_lockC1ERS_
    [A] _ZN11MallocMutex11scoped_lockC1ERS_bPb, aliases _ZN11MallocMutex11scoped_lockC2ERS_bPb
    [A] _ZN11MallocMutex11scoped_lockC2ERS_, aliases _ZN11MallocMutex11scoped_lockC1ERS_
    [A] _ZN11MallocMutex11scoped_lockC2ERS_bPb
    [A] _ZN11MallocMutex11scoped_lockD1Ev, aliases _ZN11MallocMutex11scoped_lockD2Ev
    [A] _ZN11MallocMutex11scoped_lockD2Ev
    [A] _ZN11MallocMutexC1Ev, aliases _ZN11MallocMutexC2Ev
    [A] _ZN11MallocMutexC2Ev
    [A] _ZdlPvS_
    [A] _ZnwmPv
    [A] __TBB_malloc_safer_aligned_msize
    [A] __TBB_malloc_safer_aligned_realloc
    [A] __TBB_malloc_safer_free
    [A] __TBB_malloc_safer_msize
    [A] __TBB_malloc_safer_realloc
    [A] scalable_allocation_command

================ end of changes of 'libtbbmalloc.so.2'===============

================ changes of 'libtbbmalloc_proxy.so.2'===============
  Functions changes summary: 1 Removed, 0 Changed, 3 Added functions
  Variables changes summary: 0 Removed, 0 Changed, 0 Added variable

  1 Removed function:

    [D] 'function bool __TBB_internal_find_original_malloc(int, const char**, void**)'    {__TBB_internal_find_original_malloc}

  3 Added functions:

    [A] 'function void __libc_free(void*)'    {__libc_free}
    [A] 'function void* __libc_realloc(void*, size_t)'    {__libc_realloc}
    [A] 'function size_t malloc_usable_size(void*)'    {malloc_usable_size}

================ end of changes of 'libtbbmalloc_proxy.so.2'===============

